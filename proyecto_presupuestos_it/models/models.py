from odoo import models, fields, api
class kardex_screnn(models.Model):
    _name  = 'presupuestos.proyecto'
    proyecto = fields.Many2one('project.project')
    unid_op  = fields.Char('Unidad Operativa')
    num_contrato = fields.Char('Nº Contrato')
    num_ot       = fields.Char('Nº Orden de Trabajo')
    num_ocb      = fields.Char('Nº Orden de Cambio')
    num_sp       = fields.Char('Nº Orden de Solped')
    description  = fields.Char('Descripcion')
    contratista  = fields.Many2one('res.partner')
    

