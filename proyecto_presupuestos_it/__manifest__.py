# -*- encoding: utf-8 -*-
{
    'name': 'Desarrollo de Presupuestos',
    'category': 'Uncategorized',
    'author': 'ITGRUPO',
    'depends': ['project'],
    'version': '1.0',
    'description':"""
    Desarrollo de Presupuestos
    """,
    'auto_install': False,
    'demo': [],
    'data': [
        'security/ir.model.access.csv',
        'views/presupuestos_proyecto.xml'
        ],
    'installable': True
}
